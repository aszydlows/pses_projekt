#ifndef NM_H
#define NM_H


/**===================================================================================================================*\
  @file Nm.h

  @brief Network Manager
  

  @see AUTOSAR_SWS_NetworkManagementInterface.pdf
\*====================================================================================================================*/

/*====================================================================================================================*\
    Załączenie nagłówków
\*====================================================================================================================*/

#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"	
#include "CanNm.h"
#include "FrNm.h"
#include "UdpNm.h"
#include "J1939Nm.h"
#include "GenericNm.h"
#include "ComM_Nm.h"


typedef struct {
	const Nm_BusNmType			  BusType;
	const NetworkHandleType		BusNmNetworkHandle;
	const NetworkHandleType		ComMNetworkHandle;
} Nm_ChannelType;

/** [SWS_Nm_00282]*/
typedef struct {
	const Nm_ChannelType* 		Channels;
} Nm_ConfigType;



// Functions called by NM Interface
// --------------------------------


void Nm_Init( void );


Std_ReturnType Nm_PassiveStartUp (NetworkHandleType NetworkHandle);


Std_ReturnType Nm_NetworkRequest( const NetworkHandleType NetworkHandle );


Std_ReturnType Nm_NetworkRelease( const NetworkHandleType NetworkHandle );


Std_ReturnType Nm_DisableCommunication( const NetworkHandleType NetworkHandle );


Std_ReturnType Nm_EnableCommunication( const NetworkHandleType NetworkHandle );


Std_ReturnType Nm_SetUserData (NetworkHandleType NetworkHandle, const uint8* nmUserDataPtr);


Std_ReturnType Nm_GetUserData (NetworkHandleType NetworkHandle, uint8* nmUserDataPtr);


Std_ReturnType Nm_GetPduData (NetworkHandleType NetworkHandle, uint8* nmPduData);


Std_ReturnType Nm_RepeatMessageRequest (NetworkHandleType NetworkHandle);


Std_ReturnType Nm_GetNodeIdentifier (NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr);


Std_ReturnType Nm_GetLocalNodeIdentifier (NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr);


Std_ReturnType Nm_GetState (NetworkHandleType nmNetworkHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);


void Nm_GetVersionInfo (Std_VersionInfoType* nmVerInfoPtr);


Std_ReturnType Nm_CheckRemoteSleepIndication (NetworkHandleType nmNetworkHandle, boolean* nmRemoteSleepIndPtr);


#endif /* NM_H */