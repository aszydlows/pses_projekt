/** ==================================================================================================================*\
  @file UT_NM.c

  @brief Testy jednostkowe do NM
\*====================================================================================================================*/
#include "Std_Types.h"
#include "acutest.h"
#include "fff.h"

#include "Nm.c"

DEFINE_FFF_GLOBALS;

FAKE_VOID_FUNC( CanNm_Init);
FAKE_VOID_FUNC( FrNm_Init);
FAKE_VOID_FUNC( UdpNm_Init);
FAKE_VOID_FUNC( GenericNm_Init);
FAKE_VOID_FUNC( J1939Nm_Init);

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_NetworkRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_NetworkRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_NetworkRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_NetworkRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_NetworkRequest, const NetworkHandleType );

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_PassiveStartUp, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_PassiveStartUp, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_PassiveStartUp, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_PassiveStartUp, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_PassiveStartUp, const NetworkHandleType );

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_NetworkRelease, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_NetworkRelease, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_NetworkRelease, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_NetworkRelease, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_NetworkRelease, const NetworkHandleType );

FAKE_VOID_FUNC(ComM_Nm_NetworkStartIndication, const NetworkHandleType );
FAKE_VOID_FUNC(ComM_Nm_NetworkMode, const NetworkHandleType );
FAKE_VOID_FUNC(ComM_Nm_BusSleepMode, const NetworkHandleType );
FAKE_VOID_FUNC(ComM_Nm_PrepareBusSleepMode, const NetworkHandleType );

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_DisableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_DisableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_DisableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_DisableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_DisableCommunication, const NetworkHandleType );

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_EnableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_EnableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_EnableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_EnableCommunication, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_EnableCommunication, const NetworkHandleType );

FAKE_VALUE_FUNC( Std_ReturnType, CanNm_SetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_GetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_GetPduData, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_RepeatMessageRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_GetNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_GetLocalNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_CheckRemoteSleepIndication, const NetworkHandleType , boolean* );
FAKE_VALUE_FUNC( Std_ReturnType, CanNm_GetState, const NetworkHandleType , Nm_StateType* , Nm_ModeType* );

FAKE_VALUE_FUNC( Std_ReturnType, FrNm_SetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_GetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_GetPduData, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_RepeatMessageRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_GetNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_GetLocalNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_CheckRemoteSleepIndication, const NetworkHandleType , boolean* );
FAKE_VALUE_FUNC( Std_ReturnType, FrNm_GetState, const NetworkHandleType , Nm_StateType* , Nm_ModeType* );

FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_SetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_GetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_GetPduData, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_RepeatMessageRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_GetNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_GetLocalNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_CheckRemoteSleepIndication, const NetworkHandleType , boolean* );
FAKE_VALUE_FUNC( Std_ReturnType, GenericNm_GetState, const NetworkHandleType , Nm_StateType* , Nm_ModeType* );

FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_SetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_GetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_GetPduData, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_RepeatMessageRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_GetNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_GetLocalNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_CheckRemoteSleepIndication, const NetworkHandleType , boolean* );
FAKE_VALUE_FUNC( Std_ReturnType, UdpNm_GetState, const NetworkHandleType , Nm_StateType* , Nm_ModeType* );

FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_SetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_GetUserData, const NetworkHandleType, const uint8*  );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_GetPduData, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_RepeatMessageRequest, const NetworkHandleType );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_GetNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_GetLocalNodeIdentifier, const NetworkHandleType , uint8* );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_CheckRemoteSleepIndication, const NetworkHandleType , boolean* );
FAKE_VALUE_FUNC( Std_ReturnType, J1939Nm_GetState, const NetworkHandleType , Nm_StateType* , Nm_ModeType* );

extern const Nm_ConfigType* Nm_ConfigPtr;


/**
  @brief Nm_Init test

  Function for testing Nm_Init
*/
 void Test_Of_Nm_Init(void)
{

  FFF_RESET_HISTORY();

  Nm_Init();  

  TEST_CHECK(CanNm_Init_fake.call_count == 1);
  TEST_CHECK(FrNm_Init_fake.call_count == 1);
  TEST_CHECK(UdpNm_Init_fake.call_count == 1);
  TEST_CHECK(GenericNm_Init_fake.call_count == 1);
  TEST_CHECK(J1939Nm_Init_fake.call_count == 1);
 
 }


/**
  @brief Nm_NetworkRequest test

  Function for testing Nm_NetworkRequest
*/
 void Test_Of_Nm_NetworkRequest(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;
 
  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRequest(NetworkHandle);  

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_NetworkRequest_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 7;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


  NetworkHandle = 4;
  J1939Nm_NetworkRequest_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_NetworkRequest(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRequest_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 

 }


/**
  @brief Nm_NetworkRelease test

  Function for testing Nm_NetworkRelease
*/
 void Test_Of_Nm_NetworkRelease(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_NetworkRelease_fake.return_val = E_OK;
  retv = Nm_NetworkRelease(NetworkHandle);  

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_NetworkRequest_fake.return_val = E_OK;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_NetworkRelease_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 5;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);
  

  NetworkHandle = 7;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_NetworkRelease_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_NetworkRelease(NetworkHandle);

  TEST_CHECK(CanNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(FrNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(UdpNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(GenericNm_NetworkRelease_fake.call_count == 1);
  TEST_CHECK(J1939Nm_NetworkRelease_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

/**
  @brief Nm_PassiveStartUp test

  Function for testing Nm_PassiveStartUp
*/
 void Test_Of_Nm_PassiveStartUp(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_PassiveStartUp_fake.return_val = E_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_PassiveStartUp_fake.return_val = E_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_PassiveStartUp_fake.return_val = E_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_PassiveStartUp_fake.return_val = E_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_PassiveStartUp_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_PassiveStartUp_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_PassiveStartUp(NetworkHandle);  

  TEST_CHECK(CanNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(FrNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(UdpNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(GenericNm_PassiveStartUp_fake.call_count == 1);
  TEST_CHECK(J1939Nm_PassiveStartUp_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }


/**
  @brief Nm_NetworkStartIndication test

  Function for testing Nm_NetworkStartIndication
*/
 void Test_Of_Nm_NetworkStartIndication(void)
{
  NetworkHandleType NetworkHandle;

  FFF_RESET_HISTORY();
  NetworkHandle = 0;
  Nm_NetworkStartIndication(NetworkHandle);  

  TEST_CHECK(ComM_Nm_NetworkStartIndication_fake.call_count == 1);

}

/**
  @brief Nm_NetworkMode test

  Function for testing Nm_NetworkMode
*/
 void Test_Of_Nm_NetworkMode(void)
{
  NetworkHandleType NetworkHandle;

  FFF_RESET_HISTORY();
  NetworkHandle = 0;
  Nm_NetworkMode(NetworkHandle);  

  TEST_CHECK(ComM_Nm_NetworkMode_fake.call_count == 1);

}

/**
  @brief Nm_BusSleepMode test

  Function for testing Nm_BusSleepMode
*/
 void Test_Of_Nm_BusSleepMode(void)
{
  NetworkHandleType NetworkHandle;

  FFF_RESET_HISTORY();
  NetworkHandle = 0;
  Nm_BusSleepMode(NetworkHandle);  

  TEST_CHECK(ComM_Nm_BusSleepMode_fake.call_count == 1);

}

/**
  @brief Nm_PrepareBusSleepMode test

  Function for testing Nm_PrepareBusSleepMode
*/
 void Test_Of_Nm_PrepareBusSleepMode(void)
{
  NetworkHandleType NetworkHandle;

  FFF_RESET_HISTORY();
  NetworkHandle = 0;
  Nm_PrepareBusSleepMode(NetworkHandle);  

  TEST_CHECK(ComM_Nm_PrepareBusSleepMode_fake.call_count == 1);

}


/**
  @brief Nm_DisableCommunication test

  Function for testing Nm_DisableCommunication
*/
 void Test_Of_Nm_DisableCommunication(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_DisableCommunication_fake.return_val = E_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_DisableCommunication_fake.return_val = E_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_DisableCommunication_fake.return_val = E_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_DisableCommunication_fake.return_val = E_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_DisableCommunication_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_DisableCommunication_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_DisableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_DisableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_DisableCommunication_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

 
/**
  @brief Nm_EnableCommunication test

  Function for testing Nm_EnableCommunication
*/
 void Test_Of_Nm_EnableCommunication(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_EnableCommunication_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_EnableCommunication_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_EnableCommunication(NetworkHandle);  

  TEST_CHECK(CanNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(FrNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(UdpNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(GenericNm_EnableCommunication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_EnableCommunication_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

 
/**
  @brief Nm_GetUserData test

  Function for testing Nm_GetUserData
*/
 void Test_Of_Nm_GetUserData(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  uint8* nmPduData = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData);  

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_GetUserData_fake.return_val = E_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_GetUserData_fake.return_val = E_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_GetUserData_fake.return_val = E_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_GetUserData_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_GetUserData_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_GetUserData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetUserData_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

/**
  @brief Nm_SetUserData test

  Function for testing Nm_SetUserData
*/
 void Test_Of_Nm_SetUserData(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  uint8* nmUserDataPtr = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr);  

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_SetUserData_fake.return_val = E_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_SetUserData_fake.return_val = E_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_SetUserData_fake.return_val = E_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_SetUserData_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_SetUserData_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_SetUserData(NetworkHandle, nmUserDataPtr); 

  TEST_CHECK(CanNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(FrNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(UdpNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(GenericNm_SetUserData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_SetUserData_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

/**
  @brief Nm_GetPduData test

  Function for testing Nm_GetPduData
*/
 void Test_Of_Nm_GetPduData(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  uint8* nmPduData = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData);  

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_GetPduData_fake.return_val = E_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_GetPduData_fake.return_val = E_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_GetPduData_fake.return_val = E_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_GetPduData_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_GetPduData_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_GetPduData(NetworkHandle, nmPduData); 

  TEST_CHECK(CanNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(FrNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetPduData_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetPduData_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

 
/**
  @brief Nm_RepeatMessageRequest test

  Function for testing Nm_RepeatMessageRequest
*/
 void Test_Of_Nm_RepeatMessageRequest(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_RepeatMessageRequest_fake.return_val = E_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_RepeatMessageRequest_fake.return_val = E_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_RepeatMessageRequest_fake.return_val = E_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_RepeatMessageRequest_fake.return_val = E_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_RepeatMessageRequest_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_RepeatMessageRequest_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_RepeatMessageRequest(NetworkHandle);  

  TEST_CHECK(CanNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(FrNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(UdpNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(GenericNm_RepeatMessageRequest_fake.call_count == 1);
  TEST_CHECK(J1939Nm_RepeatMessageRequest_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

/**
  @brief Nm_GetNodeIdentifier test

  Function for testing Nm_GetNodeIdentifier
*/
 void Test_Of_Nm_GetNodeIdentifier(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  uint8* nmNodeIdPtr = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr);  

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_GetNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_GetNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_GetNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_GetNodeIdentifier_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_GetNodeIdentifier_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_GetNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetNodeIdentifier_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }


/**
  @brief Nm_GetLocalNodeIdentifier test

  Function for testing Nm_GetLocalNodeIdentifier
*/
 void Test_Of_Nm_GetLocalNodeIdentifier(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  uint8* nmNodeIdPtr = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr);  

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_GetLocalNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_GetLocalNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_GetLocalNodeIdentifier_fake.return_val = E_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_GetLocalNodeIdentifier_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_GetLocalNodeIdentifier_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_GetLocalNodeIdentifier(NetworkHandle, nmNodeIdPtr); 

  TEST_CHECK(CanNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(FrNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetLocalNodeIdentifier_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetLocalNodeIdentifier_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }


/**
  @brief Nm_CheckRemoteSleepIndication test

  Function for testing Nm_CheckRemoteSleepIndication
*/
 void Test_Of_Nm_CheckRemoteSleepIndication(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  boolean* nmRemoteSleepIndPtr = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr);  

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_CheckRemoteSleepIndication_fake.return_val = E_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_CheckRemoteSleepIndication_fake.return_val = E_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_CheckRemoteSleepIndication_fake.return_val = E_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_CheckRemoteSleepIndication_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_CheckRemoteSleepIndication_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_CheckRemoteSleepIndication(NetworkHandle, nmRemoteSleepIndPtr); 

  TEST_CHECK(CanNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(FrNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(UdpNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(GenericNm_CheckRemoteSleepIndication_fake.call_count == 1);
  TEST_CHECK(J1939Nm_CheckRemoteSleepIndication_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }


/**
  @brief Nm_GetState test

  Function for testing Nm_GetState
*/
 void Test_Of_Nm_GetState(void)
{
  NetworkHandleType NetworkHandle;
  Std_ReturnType retv;

  Nm_StateType* nmStatePtr = 0;
  Nm_ModeType* nmModePtr = 0;

  FFF_RESET_HISTORY();
  retv = E_NOT_OK;
  NetworkHandle = 0;
  CanNm_EnableCommunication_fake.return_val = E_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr);  

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 0);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 1;
  retv = E_NOT_OK;
  FrNm_GetState_fake.return_val = E_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 0);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 2;
  retv = E_NOT_OK;
  UdpNm_GetState_fake.return_val = E_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 0);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 3;
  retv = E_NOT_OK;
  GenericNm_GetState_fake.return_val = E_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 0);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 4;
  J1939Nm_GetState_fake.return_val = E_OK;
  retv = E_NOT_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);


  NetworkHandle = 5;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 1);
  TEST_CHECK(retv == E_OK);

  NetworkHandle = 6;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);


    NetworkHandle = 7;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 1);
  TEST_CHECK(retv == E_NOT_OK);

  NetworkHandle = 4;
  J1939Nm_GetState_fake.return_val = E_NOT_OK;
  retv = E_OK;
  retv = Nm_GetState(NetworkHandle, nmStatePtr, nmModePtr); 

  TEST_CHECK(CanNm_GetState_fake.call_count == 1);
  TEST_CHECK(FrNm_GetState_fake.call_count == 1);
  TEST_CHECK(UdpNm_GetState_fake.call_count == 1);
  TEST_CHECK(GenericNm_GetState_fake.call_count == 1);
  TEST_CHECK(J1939Nm_GetState_fake.call_count == 2);
  TEST_CHECK(retv == E_NOT_OK);
 }

TEST_LIST = {
    { "Test of Nm_Init", Test_Of_Nm_Init },
    { "Test of Nm_NetworkRequest", Test_Of_Nm_NetworkRequest },  
    { "Test of Nm_NetworkRelease", Test_Of_Nm_NetworkRelease }, 
    { "Test of Nm_PassiveStartUp", Test_Of_Nm_PassiveStartUp },
    
    { "Test of Nm_NetworkStartIndication", Test_Of_Nm_NetworkStartIndication }, 
    { "Test of Nm_NetworkMode", Test_Of_Nm_NetworkMode },
    { "Test of Nm_BusSleepMode", Test_Of_Nm_BusSleepMode }, 
    { "Test of Nm_PrepareBusSleepMode", Test_Of_Nm_PrepareBusSleepMode },
    { "Test of Nm_DisableCommunication", Test_Of_Nm_DisableCommunication }, 
    { "Test of Nm_EnableCommunication", Test_Of_Nm_EnableCommunication },

    { "Test of Nm_SetUserData", Test_Of_Nm_SetUserData }, 
    { "Test of Nm_GetUserData", Test_Of_Nm_GetUserData },
    { "Test of Nm_GetPduData", Test_Of_Nm_GetPduData }, 
    { "Test of Nm_RepeatMessageRequest", Test_Of_Nm_RepeatMessageRequest },
    { "Test of Nm_GetNodeIdentifier", Test_Of_Nm_GetNodeIdentifier }, 
    { "Test of Nm_GetLocalNodeIdentifier", Test_Of_Nm_GetLocalNodeIdentifier },
    { "Test of Nm_CheckRemoteSleepIndication", Test_Of_Nm_CheckRemoteSleepIndication }, 
    { "Test of Nm_GetState", Test_Of_Nm_GetState },

    { NULL, NULL }                                        
};
    
