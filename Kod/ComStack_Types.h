#ifndef COMSTACK_TYPES_H_
#define COMSTACK_TYPES_H_

#define ECUC_SW_MAJOR_VERSION   1
#define ECUC_SW_MINOR_VERSION   0
#define ECUC_SW_PATCH_VERSION   0

#include "Std_Types.h" /** @req COMM820.partially */


typedef uint16 PduIdType;
typedef uint16 PduLengthType;
typedef struct {
	uint8 *SduDataPtr;			// payload
	PduLengthType SduLength;	// length of SDU
} PduInfoType;


typedef uint8 PNCHandleType; 
typedef uint8 NetworkHandleType;

#endif /*COMSTACK_TYPES_H_*/