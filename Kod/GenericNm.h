#ifndef GENERICNM_H
#define GENERICNM_H

#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"

void GenericNm_Init( void );

Std_ReturnType GenericNm_NetworkRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_NetworkRelease( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_PassiveStartUp( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_EnableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_DisableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_SetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType GenericNm_GetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType GenericNm_GetPduData( const NetworkHandleType nmChannelHandle, uint8* nmPduData);

Std_ReturnType GenericNm_RepeatMessageRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType GenericNm_GetNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType GenericNm_GetLocalNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType GenericNm_CheckRemoteSleepIndication( const NetworkHandleType nmChannelHandle,  boolean* nmRemoteSleepIndPtr);

Std_ReturnType GenericNm_GetState( const NetworkHandleType nmChannelHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);

#

#endif /* GENERICNM_H */