#ifndef J1939NM_H
#define J1939NM_H


#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"

void J1939Nm_Init( void );

Std_ReturnType J1939Nm_NetworkRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_NetworkRelease( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_PassiveStartUp( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_EnableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_DisableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_SetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType J1939Nm_GetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType J1939Nm_GetPduData( const NetworkHandleType nmChannelHandle, uint8* nmPduData);

Std_ReturnType J1939Nm_RepeatMessageRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType J1939Nm_GetNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType J1939Nm_GetLocalNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType J1939Nm_CheckRemoteSleepIndication( const NetworkHandleType nmChannelHandle,  boolean* nmRemoteSleepIndPtr);

Std_ReturnType J1939Nm_GetState( const NetworkHandleType nmChannelHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);





#endif /* J1939NM_H */