/** ==================================================================================================================*\
  @file NM.c

  @brief Implementacja  Network Manager


  @see AUTOSAR_SWS_NetworkInterface.pdf
\*====================================================================================================================*/

/*====================================================================================================================*\
    Załączenie nagłówków
\*====================================================================================================================*/

#include "Nm.h"

/*====================================================================================================================*\
    Makra lokalne
\*====================================================================================================================*/


/*====================================================================================================================*\
    Typy lokalne
\*====================================================================================================================*/

/*====================================================================================================================*\
    Zmienne globalne
\*====================================================================================================================*/

/*====================================================================================================================*\
    Zmienne lokalne (statyczne)
\*====================================================================================================================*/


/*====================================================================================================================*\
    Deklaracje funkcji lokalnych
\*====================================================================================================================*/

/*====================================================================================================================*\
    Kod globalnych funkcji inline i makr funkcyjnych
\*====================================================================================================================*/

/*====================================================================================================================*\
    Kod funkcji
\*====================================================================================================================*/



  const Nm_ChannelType channels[] = {{ NM_BUSNM_CANNM, 4, 4 }, { NM_BUSNM_FRNM, 5, 5 }, { NM_BUSNM_UDPNM, 5, 5 },{ NM_BUSNM_GENERICNM, 5, 5 }, { NM_BUSNM_J1939NM, 5, 5 }, { NM_BUSNM_LOCALNM, 5, 5 }, { 8, 5, 5 } };

  const Nm_ConfigType CoT1 = { &channels[0]};

  const Nm_ConfigType* Nm_ConfigPtr = &CoT1;

/**
  @brief Initializes the NM Interface.*/
/** [SWS_Nm_00030] */
void Nm_Init( void ){
    J1939Nm_Init();
    CanNm_Init();
    FrNm_Init();
    GenericNm_Init();    
    UdpNm_Init(); 

}

/** 
  @brief This function calls the <BusNm>_PassiveStartUp function in case NmBusType is not set to
   NM_BUSNM_LOCALNM (e.g. CanNm_PassiveStartUp function is called for NM_BUSNM_
   CANNM). 
  [SWS_Nm_00031] */

Std_ReturnType Nm_PassiveStartUp (NetworkHandleType NetworkHandle){

    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_PassiveStartUp(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_PassiveStartUp(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_PassiveStartUp(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return GenericNm_PassiveStartUp(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_PassiveStartUp(ChannelConf->BusNmNetworkHandle);
        
        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}


/** 
  @brief This function calls the <BusNm>_NetworkRequest (e.g. CanNm_NetworkRequest function is
  called if channel is configured as CAN) function in case NmBusType is not set to NM_BUSNM_
  LOCALNM. 
  [SWS_Nm_00032] */
Std_ReturnType Nm_NetworkRequest( const NetworkHandleType NetworkHandle ){

    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_NetworkRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_NetworkRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_NetworkRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return GenericNm_NetworkRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_NetworkRequest(ChannelConf->BusNmNetworkHandle);
        
        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }


}


/**
 @brief  not set to NM_BUSNM_LOCALNM (e.g. CanNm_NetworkRelease function is called if channel
  is configured as CAN). 
  [SWS_Nm_00046] */
Std_ReturnType Nm_NetworkRelease( const NetworkHandleType NetworkHandle ){
    
     const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_NetworkRelease(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_NetworkRelease(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_NetworkRelease(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return GenericNm_NetworkRelease(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_NetworkRelease(ChannelConf->BusNmNetworkHandle);
        
        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }

}


/**
 @brief  Notification that a NM-message has been received in the Bus-Sleep Mode, what indicates that
some nodes in the network have already entered the Network Mode.
 
  [SWS_Nm_00154] */
void Nm_NetworkStartIndication( const NetworkHandleType NetworkHandle ){
    
    ComM_Nm_NetworkStartIndication(NetworkHandle); /*[SWS_Nm_0015SWS_Nm_001558] */
    
}

/**
 @brief  Notification that the network management has entered Network Mode
 
  [SWS_Nm_00156] */
void Nm_NetworkMode( const NetworkHandleType NetworkHandle ){
    
    ComM_Nm_NetworkMode(NetworkHandle); /*[SWS_Nm_00158] */
    
}

/**
 @brief  Notification that the network management has entered Bus-Sleep Mode.
 
  [SWS_Nm_00162] */
void Nm_BusSleepMode( const NetworkHandleType NetworkHandle ){
    
    ComM_Nm_BusSleepMode(NetworkHandle); /*[SWS_Nm_00163] */
    
}

/**
 @brief  Notification that the network management has entered Prepare Bus-Sleep Mode.
 
  [SWS_Nm_00159] */
void Nm_PrepareBusSleepMode( const NetworkHandleType NetworkHandle ){
    
    ComM_Nm_PrepareBusSleepMode(NetworkHandle); /*[SWS_Nm_00161] */
    
}

/**
 @brief  Disables the NM PDU transmission ability. For that purpose <BusNm>_DisableCommunication
shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM (e.g. CanNm_Disable
Communication function is called if channel is configured as CAN).
 
  [SWS_Nm_00033] */
#if (NmControlEnabled == True) /*[SWS_Nm_00134] */
Std_ReturnType Nm_DisableCommunication( const NetworkHandleType NetworkHandle ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_DisableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_DisableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_DisableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return GenericNm_DisableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_DisableCommunication(ChannelConf->BusNmNetworkHandle);
        
        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}
#endif

/**
 @brief  Enables the NM PDU transmission ability. For that purpose <BusNm>_EnableCommunication
shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g. CanNm_Enable
Communication function is called if channel is configured as CAN).
 
  [SWS_Nm_00034] */
#if (NmControlEnabled == True) /*[SWS_Nm_00136] */
Std_ReturnType Nm_EnableCommunication( const NetworkHandleType NetworkHandle ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_EnableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_EnableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_EnableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return GenericNm_EnableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_EnableCommunication(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}
#endif


/**
 @brief  Set user data for NM messages transmitted next on the bus. For that purpose <BusNm>_Set
UserData shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g. Can
Nm_SetUserData function is called if channel is configured as CAN).
 
  [SWS_Nm_00035] */
#if (NmUserDataEnabled == True) /*[SWS_Nm_00138] */
Std_ReturnType Nm_SetUserData( const NetworkHandleType NetworkHandle, const uint8* nmUserDataPtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_SetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_FRNM:
			return FrNm_SetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_SetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_SetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_SetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}
#endif

/**
 @brief  Get user data out of the last successfully received NM message. For that purpose <BusNm>_
GetUserData shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g.
CanNm_GetUserData function is called if channel is configured as CAN).

  [SWS_Nm_00036] */
#if (NmUserDataEnabled == True) /*[SWS_Nm_00140] */
Std_ReturnType Nm_GetUserData( const NetworkHandleType NetworkHandle, uint8* nmUserDataPtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_GetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_FRNM:
			return FrNm_GetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_GetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_GetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_GetUserData(ChannelConf->BusNmNetworkHandle, nmUserDataPtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}
#endif

/**
 @brief  Get user data out of the last successfully received NM message. For that purpose <BusNm>_
GetUserData shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g.
CanNm_GetUserData function is called if channel is configured as CAN).

[SWS_Nm_00037] */
Std_ReturnType Nm_GetPduData( const NetworkHandleType NetworkHandle, uint8* nmPduData ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_GetPduData(ChannelConf->BusNmNetworkHandle, nmPduData);

        case NM_BUSNM_FRNM:
			return FrNm_GetPduData(ChannelConf->BusNmNetworkHandle, nmPduData);

        case NM_BUSNM_UDPNM:
			return UdpNm_GetPduData(ChannelConf->BusNmNetworkHandle, nmPduData);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_GetPduData(ChannelConf->BusNmNetworkHandle, nmPduData);

        case NM_BUSNM_J1939NM:
			return J1939Nm_GetPduData(ChannelConf->BusNmNetworkHandle, nmPduData);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}


/**
 @brief  Set Repeat Message Request Bit for NM messages transmitted next on the bus. For that
purpose <BusNm>_RepeatMessageRequest shall be called in case NmBusType is not set to
NM_BUSNM_LOCALNM. (e.g. CanNm_RepeatMessageRequest function is called if channel is
configured as CAN). This will force all nodes on the bus to transmit NM messages so that they
can be identified.

[SWS_Nm_00038] */
Std_ReturnType Nm_RepeatMessageRequest( const NetworkHandleType NetworkHandle ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_RepeatMessageRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_FRNM:
			return FrNm_RepeatMessageRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_UDPNM:
			return UdpNm_RepeatMessageRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_RepeatMessageRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_J1939NM:
			return J1939Nm_RepeatMessageRequest(ChannelConf->BusNmNetworkHandle);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}

/**
 @brief  Get node identifier out of the last successfully received NM-message. The function <BusNm>_
GetNodeIdentifier shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM.
(e.g. CanNm_GetNodeIdentifier function is called if channel is configured as CAN).

[SWS_Nm_00039] */
Std_ReturnType Nm_GetNodeIdentifier( const NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_GetNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_FRNM:
			return FrNm_GetNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_GetNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_GetNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_GetNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}


/**
 @brief  Get node identifier out of the last successfully received NM-message. The function <BusNm>_
GetNodeIdentifier shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM.
(e.g. CanNm_GetNodeIdentifier function is called if channel is configured as CAN).

[SWS_Nm_00042] */
Std_ReturnType Nm_GetLocalNodeIdentifier( const NetworkHandleType NetworkHandle, uint8* nmNodeIdPtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_GetLocalNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_FRNM:
			return FrNm_GetLocalNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_GetLocalNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_GetLocalNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_GetLocalNodeIdentifier(ChannelConf->BusNmNetworkHandle, nmNodeIdPtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}


/**
 @brief  GCheck if remote sleep indication takes place or not. For that purpose <BusNm>_CheckRemote
SleepIndication shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g.
CanNm_CheckRemoteSleepIndication function is called if channel is configured as CAN).

[SWS_Nm_00042] */
#if (NmRemoteSleepIndEnabled == True) /*[SWS_Nm_00150] */
Std_ReturnType Nm_CheckRemoteSleepIndication( const NetworkHandleType NetworkHandle, boolean* nmRemoteSleepIndPtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_CheckRemoteSleepIndication(ChannelConf->BusNmNetworkHandle, nmRemoteSleepIndPtr);

        case NM_BUSNM_FRNM:
			return FrNm_CheckRemoteSleepIndication(ChannelConf->BusNmNetworkHandle, nmRemoteSleepIndPtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_CheckRemoteSleepIndication(ChannelConf->BusNmNetworkHandle, nmRemoteSleepIndPtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_CheckRemoteSleepIndication(ChannelConf->BusNmNetworkHandle, nmRemoteSleepIndPtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_CheckRemoteSleepIndication(ChannelConf->BusNmNetworkHandle, nmRemoteSleepIndPtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}
#endif


/**
 @brief  GCheck if remote sleep indication takes place or not. For that purpose <BusNm>_CheckRemote
SleepIndication shall be called in case NmBusType is not set to NM_BUSNM_LOCALNM. (e.g.
CanNm_CheckRemoteSleepIndication function is called if channel is configured as CAN).

[SWS_Nm_00043] */
Std_ReturnType Nm_GetState( const NetworkHandleType NetworkHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr ){
    
    const Nm_ChannelType* ChannelConf = &Nm_ConfigPtr->Channels[NetworkHandle];

    switch(ChannelConf->BusType) {

        case NM_BUSNM_CANNM:
			return CanNm_GetState(ChannelConf->BusNmNetworkHandle, nmStatePtr, nmModePtr);

        case NM_BUSNM_FRNM:
			return FrNm_GetState(ChannelConf->BusNmNetworkHandle, nmStatePtr, nmModePtr);

        case NM_BUSNM_UDPNM:
			return UdpNm_GetState(ChannelConf->BusNmNetworkHandle, nmStatePtr, nmModePtr);

        case NM_BUSNM_GENERICNM:
			return  GenericNm_GetState(ChannelConf->BusNmNetworkHandle, nmStatePtr, nmModePtr);

        case NM_BUSNM_J1939NM:
			return J1939Nm_GetState(ChannelConf->BusNmNetworkHandle, nmStatePtr, nmModePtr);

        case NM_BUSNM_LOCALNM:
            return E_OK;

        default: return E_NOT_OK;
    }
}

/** This function implements the processes of the NM Interface, which need a fix cyclic scheduling.*/
/** SWS_Nm_00118 */
void Nm_MainFunction(void);