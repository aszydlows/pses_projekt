#ifndef COMNM_H
#define COMNM_H

#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"




void ComM_Nm_NetworkStartIndication( const NetworkHandleType nmChannelHandle );

void ComM_Nm_NetworkMode( const NetworkHandleType nmChannelHandle );

void ComM_Nm_BusSleepMode( const NetworkHandleType nmChannelHandle );

void ComM_Nm_PrepareBusSleepMode( const NetworkHandleType nmChannelHandle );


#endif /* COMNM_H */