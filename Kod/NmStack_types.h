#ifndef NMSTACK_TYPES_H_
#define NMSTACK_TYPES_H_


/** Operational modes of the network management */
/** [SWS_Nm_00274] */
typedef enum {
	NM_MODE_BUS_SLEEP,
	NM_MODE_PREPARE_BUS_SLEEP,
	NM_MODE_SYNCHRONIZE,
	NM_MODE_NETWORK
} Nm_ModeType;

/** States of the network management state machine */
/** [SWS_Nm_00275] */
typedef enum {
	NM_STATE_UNINIT = 0x00,
	NM_STATE_BUS_SLEEP = 0x01,
	NM_STATE_PREPARE_BUS_SLEEP = 0x02,
	NM_STATE_READY_SLEEP = 0x03,
	NM_STATE_NORMAL_OPERATION = 0x04,
	NM_STATE_REPEAT_MESSAGE = 0x05,
	NM_STATE_SYNCHRONIZE = 0x06,
	NM_STATE_OFFLINE = 0x07
} Nm_StateType;

/** BusNm Type */
/** [SWS_Nm_00276] */
typedef enum {
	NM_BUSNM_CANNM,
	NM_BUSNM_FRNM,
	NM_BUSNM_UDPNM,
	NM_BUSNM_GENERICNM,
	NM_BUSNM_UNDEF,
	NM_BUSNM_J1939NM,
	NM_BUSNM_LOCALNM
} Nm_BusNmType;


#endif /* NMSTACK_TYPES_H_ */