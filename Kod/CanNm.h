#ifndef CANNM_H
#define CANNM_H

#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"


void CanNm_Init( void );

Std_ReturnType CanNm_NetworkRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_NetworkRelease( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_PassiveStartUp( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_EnableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_DisableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_SetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType CanNm_GetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType CanNm_GetPduData( const NetworkHandleType nmChannelHandle, uint8* nmPduData);

Std_ReturnType CanNm_RepeatMessageRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType CanNm_GetNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType CanNm_GetLocalNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType CanNm_CheckRemoteSleepIndication( const NetworkHandleType nmChannelHandle,  boolean* nmRemoteSleepIndPtr);

Std_ReturnType CanNm_GetState( const NetworkHandleType nmChannelHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);

#endif /* CANNM_H */