#ifndef FRNM_H
#define FRNM_H

#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"

void FrNm_Init( void );

Std_ReturnType FrNm_NetworkRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_NetworkRelease( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_PassiveStartUp( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_EnableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_DisableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_SetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType FrNm_GetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType FrNm_GetPduData( const NetworkHandleType nmChannelHandle, uint8* nmPduData);

Std_ReturnType FrNm_RepeatMessageRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType FrNm_GetNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType FrNm_GetLocalNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType FrNm_CheckRemoteSleepIndication( const NetworkHandleType nmChannelHandle,  boolean* nmRemoteSleepIndPtr);

Std_ReturnType FrNm_GetState( const NetworkHandleType nmChannelHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);

#

#endif /* FRNM_H */