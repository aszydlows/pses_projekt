#ifndef UDPNM_H
#define UDPNM_H


#include "Std_Types.h"     
#include "ComStack_Types.h"	
#include "NmStack_Types.h"

void UdpNm_Init( void ); 

Std_ReturnType UdpNm_NetworkRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_NetworkRelease( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_PassiveStartUp( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_EnableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_DisableCommunication( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_SetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType UdpNm_GetUserData( const NetworkHandleType nmChannelHandle, const uint8* nmUserDataPtr);

Std_ReturnType UdpNm_GetPduData( const NetworkHandleType nmChannelHandle, uint8* nmPduData);

Std_ReturnType UdpNm_RepeatMessageRequest( const NetworkHandleType nmChannelHandle );

Std_ReturnType UdpNm_GetNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType UdpNm_GetLocalNodeIdentifier( const NetworkHandleType nmChannelHandle, uint8* nmNodeIdPtr);

Std_ReturnType UdpNm_CheckRemoteSleepIndication( const NetworkHandleType nmChannelHandle,  boolean* nmRemoteSleepIndPtr);

Std_ReturnType UdpNm_GetState( const NetworkHandleType nmChannelHandle, Nm_StateType* nmStatePtr, Nm_ModeType* nmModePtr);


#endif /* UDPNM_H */